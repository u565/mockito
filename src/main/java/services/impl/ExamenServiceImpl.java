package services.impl;

import models.Examen;
import repositories.ExamenRepository;
import repositories.PreguntasRepository;
import services.ExamenService;

import java.util.List;
import java.util.Optional;

public class ExamenServiceImpl implements ExamenService {

    private ExamenRepository examenRepository;
    private PreguntasRepository preguntasRepository;

    public ExamenServiceImpl(
            ExamenRepository examenRepository,
            PreguntasRepository preguntasRepository) {
        this.examenRepository = examenRepository;
        this.preguntasRepository = preguntasRepository;
    }

    @Override
    public Optional<Examen> buscarExamenPorNombre(String nombre) {

        return examenRepository
                .findAll()
                .stream()
                .filter(e -> e.getNombre().contains(nombre))
                .findFirst();
    }

    @Override
    public Examen buscarExamenConpreguntasPorNombre(String nombre) {

        Examen examen = buscarExamenPorNombre(nombre).orElse(null);

        if (examen != null) {
            List<String> preguntas = preguntasRepository.findPreguntasPorExamenId(examen.getId());
            examen.setPreguntas(preguntas);
        }

        return examen;
    }

    @Override
    public Examen guardar(Examen examen) {

        if (!examen.getPreguntas().isEmpty()) {
            preguntasRepository.guardarPreguntas(examen.getPreguntas());
        }

        return examenRepository.guardar(examen);
    }
}
