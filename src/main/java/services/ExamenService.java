package services;

import models.Examen;

import java.util.Optional;

public interface ExamenService {
    Optional<Examen> buscarExamenPorNombre(String nombre);

    Examen buscarExamenConpreguntasPorNombre(String nombre);

    Examen guardar(Examen examen);
}
