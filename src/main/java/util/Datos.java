package util;

import models.Examen;

import java.util.Arrays;
import java.util.List;

public class Datos {
    public final static List<Examen> EXAMENES = Arrays.asList(
            new Examen(1L, "Matemáticas"),
            new Examen(2L, "Lenguaje"),
            new Examen(3L, "Historia"),
            new Examen(4L, "Biología")
    );

    public final static List<Examen> EXAMENES_ID_NULL = Arrays.asList(
            new Examen(null, "Matemáticas"),
            new Examen(null, "Lenguaje"),
            new Examen(null, "Historia"),
            new Examen(null, "Biología")
    );

    public final static List<Examen> EXAMENES_ID_NEGATIVOS = Arrays.asList(
            new Examen(-1L, "Matemáticas"),
            new Examen(-2L, "Lenguaje"),
            new Examen(-3L, "Historia"),
            new Examen(-4L, "Biología")
    );

    public final static List<String> PREGUNTAS = Arrays.asList(
            "Aritmética",
            "Integrales",
            "Derivadas",
            "Geometría"
    );

    public final static Examen EXAMEN = new Examen(null, "Física");
}
