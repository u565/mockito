package repositories.impl;

import repositories.PreguntasRepository;
import util.Datos;

import java.util.List;

public class PreguntasRepositoryImpl implements PreguntasRepository {
    @Override
    public List<String> findPreguntasPorExamenId(Long id) {
        System.out.println("PreguntasRepositoryImpl.findPreguntasPorExamenId");
        return Datos.PREGUNTAS;
    }

    @Override
    public void guardarPreguntas(List<String> preguntas) {
        System.out.println("PreguntasRepositoryImpl.guardarPreguntas");
    }
}
