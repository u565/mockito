package repositories;

import java.util.List;

public interface PreguntasRepository {

    List<String> findPreguntasPorExamenId(Long id);

    void guardarPreguntas(List<String> preguntas);

}
