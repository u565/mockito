package services.impl;

import models.Examen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import repositories.ExamenRepository;
import repositories.PreguntasRepository;
import repositories.impl.ExamenRepositoryImpl;
import repositories.impl.PreguntasRepositoryImpl;
import services.ExamenService;
import util.Datos;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExamenServiceImplSpyTest {

    @Spy
    ExamenRepositoryImpl examenRepository;
    @Spy
    PreguntasRepositoryImpl preguntasRepository;

    @InjectMocks
    ExamenServiceImpl examenService;


    @Test
    void testSpy() {
        // Given
        List<String> preguntas = Arrays.asList("Aritmetica");

        // When
        //when(preguntasRepository.findPreguntasPorExamenId(anyLong())).thenReturn(preguntas);

        doReturn(preguntas).when(preguntasRepository).findPreguntasPorExamenId(anyLong());

        Examen examen = examenService.buscarExamenConpreguntasPorNombre("Biología");

        // Then
        assertEquals(4L, examen.getId());
        assertEquals(1, examen.getPreguntas().size());
    }
}