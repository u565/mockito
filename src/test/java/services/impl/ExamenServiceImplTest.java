package services.impl;

import models.Examen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import repositories.ExamenRepository;
import repositories.PreguntasRepository;
import repositories.impl.ExamenRepositoryImpl;
import repositories.impl.PreguntasRepositoryImpl;
import services.ExamenService;
import util.Datos;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ExamenServiceImplTest {

    @Mock
    ExamenRepositoryImpl examenRepository;
    @Mock
    PreguntasRepositoryImpl preguntasRepository;

    @InjectMocks
    ExamenServiceImpl service;

    @Captor
    ArgumentCaptor<Long> captorlong;

    @BeforeEach
    void setUp() {
//        MockitoAnnotations.openMocks(this);
//        examenRepository = mock(ExamenRepository.class);
//        preguntasRepository = mock(PreguntasRepository.class);
//        service = new ExamenServiceImpl(examenRepository, preguntasRepository);
    }

    @Test
    void testBuscarExamenPorNombre() {

        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);

        Optional<Examen> examen = service.buscarExamenPorNombre("Matemáticas");

        assertTrue(examen.isPresent());

        assertEquals(1L, examen.orElseThrow().getId());
        assertEquals("Matemáticas", examen.orElseThrow().getNombre());

    }

    @Test
    void testBuscarExamenPorNombreListaVacia() {

        List<Examen> examenList = Collections.emptyList();

        when(examenRepository.findAll()).thenReturn(examenList);

        Optional<Examen> examen = service.buscarExamenPorNombre("Matemáticas");

        assertTrue(examen.isEmpty());

    }

    @Test
    void testPreguntasExamen() {

        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntasRepository.findPreguntasPorExamenId(4L)).thenReturn(Datos.PREGUNTAS);
        Examen examen = service.buscarExamenConpreguntasPorNombre("Biología");

        assertEquals(4, examen.getPreguntas().size());

    }

    @Test
    void testPreguntasExamenVerify() {

        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntasRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        Examen examen = service.buscarExamenConpreguntasPorNombre("Biología");

        assertEquals(4, examen.getPreguntas().size());

        verify(examenRepository).findAll();

        verify(preguntasRepository).findPreguntasPorExamenId(examen.getId());

    }

    @Test
    void testNoExisteExamenVerify() {

        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntasRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        Examen examen = service.buscarExamenConpreguntasPorNombre("Biología");

        //assertNull(examen);

        verify(examenRepository).findAll();

        verify(preguntasRepository).findPreguntasPorExamenId(4L);

    }

    @Test
    void testGuardarExamen() {
        // Given
        Examen newExamen = Datos.EXAMEN;
        newExamen.setPreguntas(Datos.PREGUNTAS);

        when(examenRepository.guardar(any(Examen.class))).then(new Answer<Examen>() {

            Long secuencia = 5L;

            @Override
            public Examen answer(InvocationOnMock invocationOnMock) throws Throwable {
                Examen examen = invocationOnMock.getArgument(0);
                examen.setId(secuencia++);
                return examen;
            }
        });

        // When
        service.guardar(newExamen);

        // Then
        assertEquals(5L, newExamen.getId());

        verify(examenRepository).guardar(any(Examen.class));

        verify(preguntasRepository).guardarPreguntas(anyList());
    }

    @Test
    void testManejoExeptions() {
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES_ID_NULL);
        when(preguntasRepository.findPreguntasPorExamenId(isNull())).thenThrow(IllegalArgumentException.class);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            service.buscarExamenConpreguntasPorNombre("Biología");
        });

        assertEquals(IllegalArgumentException.class, exception.getClass());

        verify(examenRepository).findAll();
        verify(preguntasRepository).findPreguntasPorExamenId(isNull());
    }

    @Test
    void testArgumentMatchers() {
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntasRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);

        service.buscarExamenConpreguntasPorNombre("Biología");

        verify(examenRepository).findAll();
//        lambda
//        verify(preguntasRepository).findPreguntasPorExamenId(argThat(arg -> arg != null && arg.equals(4L)));
        verify(preguntasRepository).findPreguntasPorExamenId(eq(4L));
    }

    @Test
    void testArgumentMatchers2() {
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES_ID_NEGATIVOS);
        when(preguntasRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);

        service.buscarExamenConpreguntasPorNombre("Biología");

        verify(examenRepository).findAll();
        verify(preguntasRepository).findPreguntasPorExamenId(argThat(new MiArgumentMatcher()));
    }

    public static class MiArgumentMatcher implements ArgumentMatcher<Long> {

        @Override
        public boolean matches(Long aLong) {
            return aLong != null && aLong >= 0;
        }

        @Override
        public String toString() {
            return "Mensaje personalizado por si falla el test: El argumento debe ser mayor a cero";
        }

    }

    @Test
    void testArgumenCaptor() {
        // Given

        // When
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        service.buscarExamenConpreguntasPorNombre("Biología");
        // Then
        verify(preguntasRepository).findPreguntasPorExamenId(captorlong.capture());

        assertEquals(4L, captorlong.getValue());
    }

    @Test
    void testDoThrow() {
        // Given
        Examen examen = Datos.EXAMEN;
        examen.setPreguntas(Datos.PREGUNTAS);
        // When
        doThrow(IllegalArgumentException.class).when(preguntasRepository).guardarPreguntas(anyList());
        // Then
        assertThrows(IllegalArgumentException.class, () -> {
            service.guardar(examen);
        });
    }

    @Test
    void testDoAnswer() {
        // Given

        // When
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);

        doAnswer(invocationOnMock -> {
            Long id = invocationOnMock.getArgument(0);

            return id == 4L ? Datos.PREGUNTAS : null;
        }).when(preguntasRepository).findPreguntasPorExamenId(anyLong());

        Examen examen = service.buscarExamenConpreguntasPorNombre("Biología");

        // Then
        assertEquals(4L, examen.getId());
    }

    @Test
    void testDoAnswerGuardarExamen() {
        // Given
        Examen newExamen = Datos.EXAMEN;
        newExamen.setPreguntas(Datos.PREGUNTAS);

        doAnswer(new Answer<Examen>() {

            Long secuencia = 5L;

            @Override
            public Examen answer(InvocationOnMock invocationOnMock) throws Throwable {
                Examen examen = invocationOnMock.getArgument(0);
                examen.setId(secuencia++);
                return examen;
            }
        }).when(examenRepository).guardar(any(Examen.class));

        // When
        service.guardar(newExamen);

        // Then
        assertEquals(5L, newExamen.getId());

        verify(examenRepository).guardar(any(Examen.class));

        verify(preguntasRepository).guardarPreguntas(anyList());
    }

    @Test
    void testToCallRealMethod() {
        // Given

        // When
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        //when(preguntasRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        doCallRealMethod().when(preguntasRepository).findPreguntasPorExamenId(anyLong());

        Examen examen = service.buscarExamenConpreguntasPorNombre("Biología");

        // Then
        assertEquals(4L, examen.getId());
    }


    @Test
    void testSpy() {
        // Given
        ExamenRepository examenRepository = spy(ExamenRepositoryImpl.class);
        PreguntasRepository preguntasRepository = spy(PreguntasRepositoryImpl.class);
        ExamenService examenService = new ExamenServiceImpl(examenRepository, preguntasRepository);

        List<String> preguntas = Arrays.asList("Aritmetica");

        // When
        //when(preguntasRepository.findPreguntasPorExamenId(anyLong())).thenReturn(preguntas);

        doReturn(preguntas).when(preguntasRepository).findPreguntasPorExamenId(anyLong());

        Examen examen = examenService.buscarExamenConpreguntasPorNombre("Biología");

        // Then
        assertEquals(4L, examen.getId());
        assertEquals(1, examen.getPreguntas().size());
    }

    @Test
    void testInOrder() {
        // Given

        // When
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        service.buscarExamenConpreguntasPorNombre("Biología");
        service.buscarExamenConpreguntasPorNombre("Lenguaje");

        // Then
        InOrder inOrder = inOrder(preguntasRepository);

        inOrder.verify(preguntasRepository).findPreguntasPorExamenId(4L);
        inOrder.verify(preguntasRepository).findPreguntasPorExamenId(2L);
    }

    @Test
    void testInOrder2() {
        // Given

        // When
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        service.buscarExamenConpreguntasPorNombre("Biología");
        service.buscarExamenConpreguntasPorNombre("Lenguaje");

        // Then
        InOrder inOrder = inOrder(examenRepository, preguntasRepository);

        inOrder.verify(examenRepository).findAll();
        inOrder.verify(preguntasRepository).findPreguntasPorExamenId(4L);
        inOrder.verify(examenRepository).findAll();
        inOrder.verify(preguntasRepository).findPreguntasPorExamenId(2L);
    }

    @Test
    void testNumeroInvocaciones() {
        // Given

        // When
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        service.buscarExamenConpreguntasPorNombre("Biología");
        // Then

        verify(preguntasRepository).findPreguntasPorExamenId(anyLong());
        verify(preguntasRepository, times(1)).findPreguntasPorExamenId(anyLong());
        verify(preguntasRepository, atLeast(1)).findPreguntasPorExamenId(anyLong());
        verify(preguntasRepository, atLeastOnce()).findPreguntasPorExamenId(anyLong());
        verify(preguntasRepository, atMost(10)).findPreguntasPorExamenId(anyLong());
        verify(preguntasRepository, atMostOnce()).findPreguntasPorExamenId(anyLong());
    }

    @Test
    @Disabled
    void testNumeroInvocaciones2() {
        // Given

        // When
        when(examenRepository.findAll()).thenReturn(Datos.EXAMENES);
        service.buscarExamenConpreguntasPorNombre("Biología");
        // Then

//        verify(preguntasRepository).findPreguntasPorExamenId(anyLong());
        verify(preguntasRepository, times(2)).findPreguntasPorExamenId(anyLong());
        verify(preguntasRepository, atLeast(1)).findPreguntasPorExamenId(anyLong());
        verify(preguntasRepository, atLeastOnce()).findPreguntasPorExamenId(anyLong());
        verify(preguntasRepository, atMost(2)).findPreguntasPorExamenId(anyLong());
//        verify(preguntasRepository, atMostOnce()).findPreguntasPorExamenId(anyLong());
    }

    @Test
    void testNumeroInvocaciones3() {
        // Given

        // When
        when(examenRepository.findAll()).thenReturn(Collections.emptyList());
        service.buscarExamenConpreguntasPorNombre("Biología");
        // Then

        verify(preguntasRepository, never()).findPreguntasPorExamenId(anyLong());
        verifyNoInteractions(preguntasRepository);

        verify(examenRepository).findAll();
    }
}